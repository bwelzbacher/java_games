package com.Challenge1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String[] alphabet = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
        String input = getInput();
        String output = decryptNumber(input, alphabet);
        invertLetters(output);
    }

    private static String getInput() {
        while (true) {
            try {
                Scanner scanner = new Scanner(System.in);
                System.out.print("Input number to decode: ");
                String input = scanner.next();
                if (input.length()%2 != 0) {
                    throw new InputMismatchException();
                }
                for (int i = 0; i < input.length(); i+=2) {
                    int letterAsNum = Integer.parseInt(input.substring(i, i+2));
                    if (letterAsNum < 1 || letterAsNum > 26) {
                        throw new ArrayIndexOutOfBoundsException();
                    }
                }
                return input;
            } catch (InputMismatchException e) {
                System.out.println("Numbers must be given in pairs. Try again");
            } catch (NumberFormatException e) {
                System.out.println("Numbers only. Try again");
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("You entered a number out of bounds. Try again");
            }
        }
    }

    private static String decryptNumber(String input, String[] alphabet) {
        String output1 = "";
        for (int i = 0; i<input.length(); i+=2) {
            int letterAsNum = Integer.parseInt(input.substring(i, i+2));
            output1 += alphabet[letterAsNum - 1];
        }
        System.out.println(output1);
        return output1;
    }

    private static void invertLetters(String letters) {
        StringBuilder output2 = new StringBuilder(letters);
        System.out.println(output2.reverse());
    }
}