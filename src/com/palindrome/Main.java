package com.palindrome;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Enter a word to see if it is a palindrome. Enter 'end' to terminate.");
        outputGame();
    }

    public static void outputGame() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("Enter a word: ");
            String input = scanner.next().toLowerCase();
            if (input.equals("end")) {
                break;
            }
            System.out.println(checkForPalindrome(input));
        }
        System.out.println("Bye!");
    }

    public static String checkForPalindrome(String input) {
        int i = 0;
        for (int y = input.length() - 1; y > 0; y--) {
                if (input.charAt(i) != input.charAt(y)) {
                    return input + " is not a palindrome";
                }
                else {
                    i++;
                }
            }
        return input + " is a palindrome!";
    }
}
