package com.GoFish;

import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    private static ArrayList<String> deck = new ArrayList<>();
    private static ArrayList<String> compuHand = new ArrayList<>();
    private static ArrayList<String> playerHand = new ArrayList<>();
    private static ArrayList<String> compuBooks = new ArrayList<>();
    private static ArrayList<String> playerBooks = new ArrayList<>();
    private static String cardValue;
    private static String compuLastGuess = "";
    private static Boolean isGameOver = false;
    private static String gameOverMessage = "";

    public static void main(String[] args) {
        createDeck();
        shuffleDeck();
        dealHand();
        System.out.println("GO FISH");
        System.out.println("DIRECTIONS: Ask your opponent for a card value that is in your hand. " +
                "\n If they have it, continue asking. If not, Go Fish! (draw a card from the pond)" +
                "\n Once the deck or a hand reaches zero , the game is over. " +
                "\n Whoever has the most books wins! \n A valid input exists in your hand and must be: [2-10, J, Q, K, and A]. \n");
        while (!isGameOver) {
            playersTurn();
            System.out.println();
            computersTurn();
        }
        gameOver();
    }

    private static String getCardValue(ArrayList<String> cardPile, int index) {
        String value = Character.toString(cardPile.get(index).charAt(0));
        if (value.equals("1")) {
            value = "10";
        }
        return value;
    }

    private static void checkForBooks(ArrayList<String> player) {
        String playersName = player == compuHand ? "The Computer" : "You";
        int pairCounter = 1;
        for (int i=0; i<player.size()-1; i++) {
            if (player.get(i).charAt(0) == player.get(i+1).charAt(0)){
                pairCounter++;
                if (pairCounter == 4) {
                    String set = getCardValue(player, i);
                    System.out.println(playersName + " got a book of " + set + "'s!");
                    if (player == playerHand) {
                        playerBooks.add(set);
                    } else {
                        compuBooks.add(set);
                    }
                    i -= 2;
                    player.remove(i);
                    player.remove(i);
                    player.remove(i);
                    player.remove(i);
                }
            }
            else {
                pairCounter = 1;
            }
        }
    }

    private static void checkForGameOver() {
        if (deck.size() == 0 || playerHand.size() == 0 || compuHand.size() == 0) {
            isGameOver = true;
        }
        gameOverMessage = deck.size() == 0 ? "The pond ran out of fish." : " ran out of hooks.";
        String playerWithZero = playerHand.size() == 0 ? "You" : "The computer";
        gameOverMessage = gameOverMessage.contains("hooks") ? playerWithZero + gameOverMessage : gameOverMessage;
    }

    private static void gameOver() {
        System.out.println("GAME OVER! " + gameOverMessage);
        configureGameStandings();
        showGameStandings();
        if (playerBooks.size() > compuBooks.size()) {
            System.out.println("YOU WON! " + playerBooks.size() + " - " + compuBooks.size());
        } else if (playerBooks.size() == compuBooks.size()) {
            System.out.println("It was a tie! " + playerBooks.size() + " - " + compuBooks.size());
        } else {
            System.out.println("The Computer beat you, " + playerBooks.size() + " - " + compuBooks.size()+ ", Better luck next time!");
        }
    }

    private static void userInput() {
        while(true) {
            try {
                Scanner scanner = new Scanner(System.in);
                ArrayList<String> valuesInHand = valuesInHand(playerHand);
                System.out.print("Computer, Do you have any... ");
                cardValue = scanner.next().toUpperCase();
                if (!valuesInHand.contains(cardValue)) {
                    throw new InputMismatchException();
                }
                return;
            } catch (InputMismatchException e) {
                System.out.println("That is not a valid input. Try again:");
            }
        }
    }

    private static void checkOpponentForCard(ArrayList<String> opponent, ArrayList<String> player) {
        Boolean isAMatch = false;
        ArrayList<Integer> cardsToRemove = new ArrayList<>();
        String playersName = opponent == compuHand ? "The Computer" : "You";

        for (String card : opponent) {
            if (card.contains(cardValue)) {
                player.add(card);
                cardsToRemove.add(opponent.indexOf(card));
                isAMatch = true;
            }
        }
        int cardsRemoved = 0;
        for (int i=0; i<cardsToRemove.size(); i++) {
            opponent.remove(cardsToRemove.get(i) - cardsRemoved);
            cardsRemoved++;
        }
        if (!isAMatch) {
            System.out.println(playersName + " said GO FISH!");
            drawACard(player);
            checkForGameOver();
        } else {
            System.out.println("YES! " + playersName + " did have " + cardValue.toUpperCase() + "'s");
            repeatTurn(player);
        }
    }

    private static void computersTurn() {
        checkForGameOver();
        if (isGameOver) {
            return;
        }
        ArrayList<String> valuesInHand = valuesInHand(compuHand);
        do {
            int randomIndex = (int) (Math.random() * compuHand.size());
            cardValue = getCardValue(compuHand, randomIndex);
        } while(compuLastGuess.equals(cardValue) && valuesInHand.size()>1);
        compuLastGuess = cardValue;
        System.out.println("DO YOU HAVE ANY " + cardValue.toUpperCase() + "'s?");
        checkOpponentForCard(playerHand, compuHand);
    }

    private static void playersTurn() {
        configureGameStandings();
        checkForGameOver();
        if (isGameOver) {
            return;
        }
        showGameStandings();
        userInput();
        checkOpponentForCard(compuHand, playerHand);
    }

    private static void drewWhatAskedText(ArrayList<String> player) {
        String playersName = player == compuHand ? "The Computer" : "You";
        String word = player == compuHand ? "they" : "you";
        String addS = player == compuHand ? "s" : "";
        System.out.println(playersName + " drew what "+ word + " asked for. " + playersName + " get" + addS + " to go again!");
    }

    private static void drawACard(ArrayList<String> player) {
        if (deck.size() > 0) {
            player.add(deck.get(0));
            String value = getCardValue(deck, 0);
            if (cardValue.equals(value)) {
                drewWhatAskedText(player);
                deck.remove(0);
                repeatTurn(player);
            } else if (player == playerHand) {
                System.out.println("You drew a " + deck.get(0));
                deck.remove(0);
            } else {
                deck.remove(0);
            }
        } else {
            isGameOver = true;
        }
    }

    private static void repeatTurn(ArrayList<String> player) {
        if (player == playerHand) {
            playersTurn();
        } else {
            computersTurn();
        }
    }

    private static void createDeck() {
        String[] suits = {"\u2660", "\u2764", "\u2666", "\u2663"};
        String[] values = {"A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"};
        for (String value : values) {
            for (String suit : suits) {
                deck.add(value + suit);
            }
        }
    }

    private static void shuffleDeck() {
        for (int i=0; i<deck.size(); i++) {
            int randomNum = (int)(Math.random() * 52);
            String movingCard = deck.get(i);
            deck.set(i, deck.get(randomNum));
            deck.set(randomNum, movingCard);
        }
    }

    private static void dealHand() {
        for (int i=0; i<6; i++) {
            playerHand.add(deck.get(0));
            deck.remove(0);
            compuHand.add(deck.get(0));
            deck.remove(0);
        }
    }

    private static ArrayList<String> sortHand(ArrayList<String> player) {
        Collections.sort(player);
        return player;
    }

    private static ArrayList<String> valuesInHand(ArrayList<String> player) {
        ArrayList<String> values = new ArrayList<>();
        for (String card : player) {
            String value = getCardValue(player, player.indexOf(card));
           if (!values.contains(value)){
               values.add(value);
            }
        }
        return values;
    }

    private static void configureGameStandings() {
        playerHand = sortHand(playerHand);
        compuHand = sortHand(compuHand);
        checkForBooks(playerHand);
        checkForBooks(compuHand);
    }

    private static void showGameStandings() {
        System.out.println();
        System.out.println("Your Books: " + playerBooks);
        System.out.println("The Computer's Books: " + compuBooks);
        if (!isGameOver) {
            System.out.println("Your hand: " + playerHand);
        }
        System.out.println();
    }
}
