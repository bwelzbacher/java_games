package com.Challenge2;

import javax.xml.crypto.Data;
import java.util.ArrayList;
import java.util.Collections;

public class Main {

    public static void main(String[] args) {
        Animals row1 = new Animals("A1", "Cat", "04") {};
        Animals row2 = new Animals("A2", "Horse", "02") {};
        Animals row3 = new Animals("A3", "Elephant", "01") {};
        Animals row4 = new Animals("A4", "Dog", "03") {};

        ArrayList<Animals> dataSet = new ArrayList<>();
        dataSet.add(row1);
        dataSet.add(row2);
        dataSet.add(row3);
        dataSet.add(row4);

        Collections.sort(dataSet);
        System.out.println(dataSet);
        Collections.sort(dataSet,Animals.DescComparator);
        System.out.println(dataSet);

    }

}