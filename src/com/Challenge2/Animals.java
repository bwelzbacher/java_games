package com.Challenge2;

import javax.xml.crypto.Data;
import java.util.Comparator;

class Animals implements Comparable<Animals> {
    private String code;
    private String desc;
    private String order;

    public String getDesc() {
        return desc;
    }

    public String getCode() {
        return code;
    }

    public String getOrder() {
        return order;
    }

    public Animals(String code, String desc, String order) {
        this.code = code;
        this.desc = desc;
        this.order = order;
    }

    @Override
    public int compareTo(Animals o) {
        return 0;
    }

    public static Comparator<Animals> DescComparator = new Comparator<Animals>() {

        @Override
        public int compare(Animals e1, Animals e2) {
            return e1.getDesc().compareTo(e2.getDesc());
        }
    };

    public String toString() {
        return this.desc;
    }
}
