package com.connect4;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);
    private static boolean isGameOver = false;
    private static String currentPlayer = "X";
    private static String[] spaces = {" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ",
            " ", " ", " ", " ", " ", " ", " ", " "," ", " "," ", " ",
            " ", " "," ", " "," ", " "," ", " "," ", " "," ", " ",};
    private static ArrayList<Integer> disabledColumns = new ArrayList<Integer>();
    public static final String GREEN = "\u001B[32m";
    public static final String BLUE = "\u001B[36m";
    public static final String RESET = "\u001B[0m";
    public static final String RED = "\u001B[31m";

    public static void main(String[] args) {
        startGame();
        while(!isGameOver) {
            int indexPlayedAt = updateBoard(userInput(), currentPlayer);
            printBoard();
            isGameOver = checkForWin(indexPlayedAt);
            togglePlayer();
            disableFilledColumn(indexPlayedAt);
        }

    }

    private static void startGame() {
        System.out.println("Connect Four");
        printBoard();
    }

    private static boolean checkForWin(int index) {
        return checkForRowWin(index) || checkForColumnWin(index) || checkForDiagonalWin(index);
    }

    private static boolean checkForRowWin(int index) {
        int inARow = 1;
        int spacesFromLeft = index%6;
        index -= spacesFromLeft;
        for (int i=0; i<5; i++) {
            if (!spaces[index].equals(" ") && spaces[index].equals(spaces[index+1])) {
                inARow++;
                if (inARow == 4) {
                    System.out.println("Four In A Row! " + currentPlayer + "'s Win!");
                    return true;
                }
            } else {
                inARow = 1;
            }
            index++;
        }
        return false;
    }

    private static boolean checkForColumnWin(int index) {
        int inAColumn = 1;
        if(index < 18) {
            for (int i = 0; i < 3; i++) {
                if (spaces[index].equals(spaces[index + 6])) {
                    index += 6;
                    inAColumn++;
                    if (inAColumn == 4) {
                        System.out.println("Four In A Row! " + currentPlayer + "'s Win!");
                        return true;
                    }
                } else {
                    break;
                }
            }
        }
        return false;
    }

    private static boolean checkForDiagonalWin(int index) {
        int inADiagonal = 1;
        int initialIndex = index;
        while (index%6 != 0 && index >= 7) {
            if ((index+1)%6 == 0) {
                index += 7;
                break;
            }
            index -= 7;
        }

        for (int i=0; i<5; i++) {
            if (index <= 28 && spaces[index] != " " && spaces[index].equals(spaces[index + 7]) && index != 3 && index != 4 && index != 5) {
                inADiagonal++;
            } else {
                inADiagonal = 1;
            }
            index += 7;
            if (inADiagonal == 4) {
                System.out.println("Four In A Row! " + currentPlayer + "'s Win!");
                return true;
            }
        }

        index = initialIndex;
        inADiagonal = 1;
        while ((index+1)%6 != 0 && index >= 5) {
            index -= 5;
        }
        for (int i=0; i <5; i++) {
            if (index <= 29 && spaces[index] != " " && spaces[index].equals(spaces[index + 5])) {
                inADiagonal++;
            } else {
                inADiagonal = 1;
            }
            index += 5;
            if (inADiagonal == 4) {
                System.out.println("Four In A Row! " + currentPlayer + "'s Win!");
                return true;
            }
        }
        return false;
    }


    private static void togglePlayer() {

        currentPlayer = currentPlayer == "X" ?  "O" : "X";
    }

    private static int updateBoard(int columnNum, String player) {
        for (int i=spaces.length -1; i>= 0; i--){
            if ((i-columnNum+1)%6 == 0 && spaces[i] != "X" && spaces[i] != "O") {
                spaces[i] = player;
                return i;
            }
        }
        return 0;
    }

    private static int userInput() {
        while(true) {
            try {
                System.out.print("Player " + currentPlayer + "- Enter a column (1-6) to place your piece: ");
                String input = scanner.next();
                int inputInt = Integer.parseInt(input);
                if (inputInt < 1 || inputInt > 6) {
                    throw new NumberFormatException();
                }
                for (int i=0; i<disabledColumns.size(); i++) {
                    if (inputInt == (disabledColumns.get(i)+1)) {
                        throw new InputMismatchException();
                    }
                }
                return inputInt;
            }
            catch (NumberFormatException e) {
                System.out.println(RED + "Please enter a number 1 through 6 only." + RESET);
                System.out.println();
            }
            catch (InputMismatchException e) {
                    System.out.println(RED + "That column is full. Try again" + RESET);
                    System.out.println();
            }
        }
    }

    private static String setPieceColor(int index) {
        return spaces[index].equals("X") ? RED + spaces[index] + GREEN : BLUE + spaces[index] + GREEN;
    }

    private static void disableFilledColumn(int index) {
        if (index < 6) {
            disabledColumns.add(index%6);
        }
        if (disabledColumns.size() == 6) {
            System.out.println("It's a tie!");
            isGameOver = true;
        }
    }

    private static void printBoard(){
        System.out.println("   1   2   3   4   5   6");
        System.out.println(GREEN + " ------------------------- ");
        System.out.println("| (" + setPieceColor(0) + ") (" + setPieceColor(1) + ") ("  + setPieceColor(2) + ") ("  + setPieceColor(3) + ") (" + setPieceColor(4) + ") (" + setPieceColor(5) + ") |");
        System.out.println("| (" + setPieceColor(6) + ") (" + setPieceColor(7) + ") ("  + setPieceColor(8) + ") ("  + setPieceColor(9) + ") (" + setPieceColor(10)+ ") (" + setPieceColor(11) + ") |");
        System.out.println("| (" + setPieceColor(12) + ") (" + setPieceColor(13) + ") ("  + setPieceColor(14) + ") ("  + setPieceColor(15) + ") (" + setPieceColor(16) + ") (" + setPieceColor(17) + ") |");
        System.out.println("| (" + setPieceColor(18) + ") (" + setPieceColor(19) + ") ("  + setPieceColor(20) + ") ("  + setPieceColor(21) + ") (" + setPieceColor(22) + ") (" + setPieceColor(23) + ") |");
        System.out.println("| (" + setPieceColor(24) + ") (" + setPieceColor(25) + ") ("  + setPieceColor(26) + ") ("  + setPieceColor(27) + ") (" + setPieceColor(28) + ") (" + setPieceColor(29) + ") |");
        System.out.println("| (" + setPieceColor(30) + ") (" + setPieceColor(31) + ") ("  + setPieceColor(32) + ") ("  + setPieceColor(33) + ") (" + setPieceColor(34) + ") (" + setPieceColor(35) + ") |");
        System.out.println(" ------------------------- "  + RESET);

    }

}