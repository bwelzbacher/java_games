package com.PFZ;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    static private Scanner scanner = new Scanner(System.in);
    static private String type = "";
    static private String typeLong = "";
    static private int guessLength;
    static private ArrayList guessWord = new ArrayList();
    static private ArrayList guessWordOG = new ArrayList();
    static private ArrayList userGuess  = new ArrayList();
    static private boolean gameOver = false;
    static private String pfzOutput = "";
    static private int attempts = 1;

    public static void main(String[] args) {
        startGame();
        generateRandom();
        while (!gameOver) {
            userGuess.clear();
            getUserGuess();
            calculateCorrect();
            pfzOutput = "";
            guessWord = (ArrayList) guessWordOG.clone();
            attempts++;
        }
    }

    private static void startGame() {
        System.out.println("Let's play PFZ!");
        System.out.print("Play with letters or numbers? (L or N): ");
        type = scanner.next().toUpperCase();
        switch(type) {
            case ("L"):
                System.out.println("Letters it is!");
                typeLong = "letters";
                break;
            case ("N"):
                System.out.println("Numbers it is!");
                typeLong = "numbers";
                break;
            default:
                System.out.println("I didn't understand that. We'll just go with numbers");
                typeLong = "numbers";
                break;
        }

        while (true) {
            try {
                System.out.print("Length? (2-10): ");
                String input = scanner.next();
                guessLength = Integer.parseInt(input);
                if  (guessLength >= 2 && guessLength <= 10) {
                    System.out.println("Alright. So you're guessing " + guessLength + " " + typeLong + ". Let's do it!");
                    break;
                } else {
                    System.out.println("How about trying a number 2 through 10?");
                }
            } catch (NumberFormatException e) {
                System.out.println("I said a number between 2 and 10...Try again");
            }
        }
    }

    private static void generateRandom() {
        if (type.equals("L")) {
           String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
           for (int i = 0; i < guessLength; i++) {
               int letterIndex = (int) (Math.random() * 27 - 1);
               if (guessWord.contains(letterIndex)) {
                   i--;
               } else {
                   guessWord.add(Character.toString(alphabet.charAt(letterIndex)));
               }
           }
        }
        else {
            for (int i = 0; i < guessLength; i++) {
                String randomNum = Long.toString(Math.round(Math.random() * 9));
                if (guessWord.contains(randomNum)) {
                    i--;
                } else {
                    guessWord.add(randomNum);
                }
            }
        }
            guessWordOG = (ArrayList) guessWord.clone();
    }

    private static void getUserGuess() {
        while(true) {
            try {
                System.out.print("What's your guess? ");
                String guess = scanner.next();
                if (guess.length() != guessLength) {
                    throw new IndexOutOfBoundsException();
                }
                for (int i = 0; i < guess.length(); i++) {
                    userGuess.add(Character.toString(guess.charAt(i)).toUpperCase());
                }
                break;
            } catch (IndexOutOfBoundsException e) {
                System.out.println("Um. You said " + guessLength + " " + typeLong + ". Try again");
            }
        }
    }

    private static void calculateCorrect() {
        boolean notZ = false;

        for (int i=0; i<guessWord.size(); i++) {
            if (guessWord.get(i).equals(userGuess.get(i))) {
                notZ = true;
                pfzOutput += "F";
                guessWord.set(i, "!");
                userGuess.set(i, "?");
            } else if (userGuess.contains(guessWord.get(i))) {
                pfzOutput += "P";
                notZ = true;
            }
        }

        if (!notZ) {
            pfzOutput = "Zilch!";
        } else {
            formatPFZ();
        }
        System.out.println(pfzOutput);
        checkForGamesEnd();
    }

    private static void formatPFZ() {
        char tempPfzArray[] = pfzOutput.toCharArray();
        Arrays.sort(tempPfzArray);
        StringBuilder builder = new StringBuilder();
        for (char value : tempPfzArray) {
            builder.append(value);
        }
        pfzOutput = builder.toString();
    }

    private static void checkForGamesEnd() {
        String neededForWin = "";
        for (int i=0; i<guessLength; i++) {
            neededForWin += "F";
        }
        if (pfzOutput.equals(neededForWin)) {
            gameOver = true;
        }
        if (gameOver) {
            System.out.println("Good job! You won in " + attempts + " attempts!");
        }
    }

}
