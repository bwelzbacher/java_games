package com.tictactoe;

import com.sun.org.apache.regexp.internal.RE;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.InputMismatchException;

public class Main {
    private static int player = 1;
    private static ArrayList<String> boardContent = new ArrayList<>(9);
    private static int turns = 1;
    public static final String GREEN = "\u001B[32m";
    public static final String BLUE = "\u001B[36m";
    public static final String RESET = "\u001B[0m";
    public static final String RED = "\u001B[31m";




    public static void main(String[] args) {
        boolean isGameOver = false;
        for (int i=0; i<9; i++) {
            boardContent.add(Integer.toString(i + 1));
        }
        System.out.println("TIC TAC TOE");
        System.out.println();
        printBoard();
        while(!isGameOver) {
            playerInput(player);
            printBoard();
            if (turns >= 5) {
                isGameOver = checkForWin();
                if (!isGameOver && turns == 10) {
                    System.out.println("It's A Scratch, Game Over!");
                } else if (isGameOver) {
                    printWinner();
                }
            }
         }
    }

    private static void playerInput(int currentPlayer) {
        Scanner scanner = new Scanner(System.in);
        boolean isValid = false;
        int userInput;
        System.out.println();
        System.out.print("Player " + currentPlayer + " Pick an available space (1-9): ");
        try {
            userInput = scanner.nextInt();
            System.out.println();
            if (userInput < 1 || userInput > 9) {
                throw new InputMismatchException();
            }
            for (int i=0; i < boardContent.size(); i++) {
                if (Integer.toString(userInput).equals(boardContent.get(i))) {
                    if (currentPlayer == 1) {
                        boardContent.set(Integer.valueOf(Integer.toString(userInput-1)), "X");
                    } else {
                        boardContent.set(Integer.valueOf(Integer.toString(userInput-1)), "O");
                    }
                    turns++;
                    isValid = true;
                    break;
                }
            }
            if (!isValid) {
                throw new NumberFormatException();
            }
            player = togglePlayers(currentPlayer);
        } catch (InputMismatchException e){
            System.out.println("Incorrect Input. Please enter a number 1-9");
        } catch (NumberFormatException e) {
            System.out.println("This spot has been taken. Please select another number");
        }
    }

    private static int togglePlayers(int player) {
       return player==1 ?  2 : 1;
    }

    private static boolean checkForWin() {
        return boardContent.get(0).equals(boardContent.get(1)) && boardContent.get(1).equals(boardContent.get(2)) ||
        boardContent.get(3).equals(boardContent.get(4)) && boardContent.get(4).equals(boardContent.get(5)) ||
        boardContent.get(6).equals(boardContent.get(7)) && boardContent.get(7).equals(boardContent.get(8)) ||
        boardContent.get(0).equals(boardContent.get(3)) && boardContent.get(3).equals(boardContent.get(6)) ||
        boardContent.get(1).equals(boardContent.get(4)) && boardContent.get(4).equals(boardContent.get(7)) ||
        boardContent.get(2).equals(boardContent.get(5)) && boardContent.get(5).equals(boardContent.get(8)) ||
        boardContent.get(0).equals(boardContent.get(4)) && boardContent.get(4).equals(boardContent.get(8)) ||
        boardContent.get(2).equals(boardContent.get(4)) && boardContent.get(4).equals(boardContent.get(6));
    }

    private static void printWinner() {
        player = togglePlayers(player);
        System.out.println("Player " + player + " Wins!!");
    }

    private static String setColor(int index) {
        if (boardContent.get(index) == "X") {
            return GREEN + boardContent.get(index) + RESET;
        } else if (boardContent.get(index) == "O"){
            return BLUE + boardContent.get(index) + RESET;
        } else {
            return boardContent.get(index);
        }
    }

    private static String verticalLine() {
        return RED + " | " + RESET;
    }


    private static void printBoard() {
        System.out.println(" " + setColor(0) + verticalLine() + setColor(1)  + verticalLine() + setColor(2));
        System.out.println(RED + "___ ___ ___" + RESET);
        System.out.println(" " + setColor(3) + verticalLine() + setColor(4)  + verticalLine() + setColor(5));
        System.out.println(RED + "___ ___ ___" + RESET);
        System.out.println(" " + setColor(6) + verticalLine() + setColor(7)  + verticalLine() + setColor(8));
    }
}
